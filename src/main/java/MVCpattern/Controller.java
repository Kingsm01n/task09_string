package MVCpattern;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Controller {
    public static String stringUtils(Object... o){
        String strConcat = "";
        for(Object arg : o){
            strConcat = strConcat.concat(String.valueOf(arg));
        }
        return strConcat;
    }
    public static void regEx(String str){
        if(str.matches("(?u)^[A-Z].*")){
            View.display("Starts with upper case");
        } else {
            View.display("Starts with down case");
        }
    }
}
