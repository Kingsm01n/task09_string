package MVCpattern;

import static MVCpattern.Controller.regEx;
import static MVCpattern.Controller.stringUtils;

public class Application {
    public static void main(String[] args) {
        String str = stringUtils("S the assda", 214, "ceax", 232.12);
        View.display(str);
        regEx(str);
    }
}
